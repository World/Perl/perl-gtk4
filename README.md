Gtk4
====

Perl bindings to the 4.x series of the [GTK][gtk] graphical user interface
toolkit.

This module allows you to GUI applications using the GTK toolkit in a Perlish
and object-oriented way, freeing you from the casting and memory management in
C, yet remaining very close in spirit to original API.

[gtk]: https://www.gtk.org

INSTALLATION
------------

To install this module from Git, you will need [dzil][dzil] installed

```
   dzil build
   dzil install
```

[dzil]: https://metacpan.org/pod/Dist::Zilla

DEPENDENCIES
------------

GTK needs the following C library, as well as its dependencies:

  - GTK ≥ 4.0

and these Perl modules:

  - Glib::Object::Introspection
  - Cairo::GObject
  - Glib::IO
  - Pango

COPYRIGHT AND LICENSE
---------------------

Copyright 2022 Emmanuele Bassi

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.
